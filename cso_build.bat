@echo off
 
:: 1) Path to MinGW\bin
:: 2) Path to Digital Mars
SET PATH=%PATH%;C:/Tools/dm/bin;
SET ZLIB_PATH=C:/temp/zlib-1.2.8
SET INSTALL_DIR=Out
:: ---------------------------------------------------------------
 
:: Delete object files from previous build
del /S zlib-1.2.8\*.o curl-7.45.0\*.o curl-7.45.0\*.res
 
echo 'Building zlib'
cd zlib-1.2.8
mingw32-make -fwin32/Makefile.gcc
cd ..
 
echo 'Building curl'
cd curl-7.45.0
mingw32-make -C lib -f Makefile.m32 CFG=mingw32-winssl-zlib-ipv6 LDFLAGS=-static
strip -s lib\libcurl.dll
 
echo 'Creating output dir'
mkdir %INSTALL_DIR%
copy lib\libcurl.* %INSTALL_DIR%
cd ..
 
echo 'Job finished'
 
:: Build sample
::SET PATH=%INSTALL_DIR%\bin
::dmd app.d -ofapp32.exe
::app32