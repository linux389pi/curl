@echo off
 
:: 1) Path to MinGW\bin
:: 2) Path to Digital Mars
SET PATH=C:/Qt/5.5/mingw492_32/bin;C:\Tools\dm\bin;
SET ZLIB_PATH=C:/temp/zlib-1.2.8
SET INSTALL_DIR=C:/temp/curl-7.45.0/Out
:: ---------------------------------------------------------------
 
:: Delete object files from previous build
del /S zlib-1.2.8\*.o curl-7.45.0\*.o curl-7.45.0\*.res
 
cd zlib-1.2.8
mingw32-make -fwin32/Makefile.gcc
cd ..
 
cd curl-7.45.0
mingw32-make -C lib -f Makefile.m32 CFG=mingw32-winssl-zlib-ipv6 LDFLAGS=-static
strip -s lib\libcurl.dll
 
mkdir %INSTALL_DIR%\bin %INSTALL_DIR%\lib
copy lib\libcurl.dll %INSTALL_DIR%\bin
implib /system %INSTALL_DIR%\lib\curl.lib lib\libcurl.dll
cd ..
 
:: Build sample
SET PATH=%INSTALL_DIR%\bin
dmd app.d -ofapp32.exe
app32